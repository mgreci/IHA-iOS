//
//  PatientDetailsViewController.swift
//  IHA
//
//  Created by Matthew Greci on 2/21/18.
//  Copyright © 2018 Matthew Greci. All rights reserved.
//

import UIKit

class PatientDetailsViewController: UIViewController {
    
    @IBOutlet weak var patientLocation: UITextField!
    @IBOutlet weak var patientDischargeDate: UITextField!
    
    func referralDataIsNotValid(referral: ([String: String])) -> Bool {
        //check required fields on PatientDetailsViewController
        return true;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        Shared.shared.patientLocation = self.patientLocation.text!
        Shared.shared.patientDischargeDate = self.patientDischargeDate.text!
    }
    
}
