//
//  ContactViewController.swift
//  IHA
//
//  Created by Matthew Greci on 2/21/18.
//  Copyright © 2018 Matthew Greci. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
