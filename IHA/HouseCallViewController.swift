//
//  HouseCallViewController.swift
//  IHA
//
//  Created by Matthew Greci on 2/1/18.
//  Copyright © 2018 Matthew Greci. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedOutside() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

class HouseCallViewController: UIViewController {
    
    func errorResponse(error: Any?, message: String) {
        print("error=\(String(describing: error))")
        let alertView = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertView.addAction(ok)
        DispatchQueue.main.async { [unowned self] in
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    func successfulResponse(data: Data?) {
        let alertView = UIAlertController(title: "Request Submitted!", message: "Your request for a hosue call has been submitted and IHA will contact you soon.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: {(alert: UIAlertAction!) in self.dismiss(animated: true, completion: nil)})
        alertView.addAction(ok)
        DispatchQueue.main.async { [unowned self] in
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    func alertUser(text: String) {
        let alertView = UIAlertController(title: "Missing Information", message: text, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertView.addAction(ok)
        DispatchQueue.main.async { [unowned self] in
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    func isInformationValid(data: [String: String]) -> Bool {
        if (data["firstName"] == "") {
            alertUser(text: "First name is missing.")
            return false;
        }
        if (data["lastName"] == "") {
            alertUser(text: "Last name is missing.")
            return false;
        }
        if (data["phoneNumber"] == "" || data["phoneNumber"]?.count != 10) {
            alertUser(text: "Phone number must be 10 digits with no spaces, dashes, or parentheses.")
            return false;
        }
        return true;
    }
    
    func submitRequest() {
        // 1. Verify data is valid
        let json: [String: String] = [
            "firstName": self.firstName.text!,
            "lastName": self.lastName.text!,
            "phoneNumber": self.phoneNumber.text!,
            "referralName": "",
            "referralPhoneNumber": "",
            "dischargeDate": "",
            "location": "",
            "referralOrg": "",
            "source": "iOS"
        ];
        
        // 2. Verify data is valid
        if (!isInformationValid(data: json)) {
            print("referral data is NOT valid: \(String(describing: json))")
            return
        }
        
        // 3. Create request object
        let url: String = "https://iha-referral.meteorapp.com/methods/requestHouseCallAPI";
        //let url: String = "http://localhost:3000/methods/requestHouseCallAPI";
        var request = URLRequest(url: URL(string: url)!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        request.httpBody = jsonData
        
        // 4. Send HTTP POST to API
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // 5. Display result from request
            //network error
            guard let data = data, error == nil else {
                self.errorResponse(error: error, message: "There was a networking error. Please ensure you are connected to the internet and try again.")
                return
            }
            
            //HTTP error
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                self.errorResponse(error: error, message: "There was error with the server. Please try again later.")
            } else {
                //successful response
                self.successfulResponse(data: data)
            }
        }
        task.resume()
    }
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!

    @IBAction func submitRequest(_ sender: Any) {
        self.submitRequest();
    }
    @IBAction func cancelRequest(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.hideKeyboardWhenTappedOutside()
    }
    
}
