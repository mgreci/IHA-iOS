//
//  ReferralViewController.swift
//  IHA
//
//  Created by Matthew Greci on 2/21/18.
//  Copyright © 2018 Matthew Greci. All rights reserved.
//

import UIKit

let defaults = UserDefaults.standard

class ReferralViewController: UIViewController {
    
    func errorResponse(error: Any?, message: String) {
        print("error=\(String(describing: error))")
        let alertView = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertView.addAction(ok)
        DispatchQueue.main.async { [unowned self] in
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    func successfulResponse(data: Data?) {
        let alertView = UIAlertController(title: "Referral Submitted!", message: "Your referral has been submitted and IHA will be in contact soon.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: {(alert: UIAlertAction!) in self.dismiss(animated: true, completion: nil)})
        alertView.addAction(ok)
        DispatchQueue.main.async { [unowned self] in
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    func alertUser(text: String) {
        let alertView = UIAlertController(title: "Missing Information", message: text, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertView.addAction(ok)
        DispatchQueue.main.async { [unowned self] in
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    func isInformationValid(data: [String: String]) -> Bool {
        if (data["firstName"] == "") {
            alertUser(text: "The patient first name is missing.")
            return false;
        }
        if (data["lastName"] == "") {
            alertUser(text: "The patient last name is missing.")
            return false;
        }
        if (data["phoneNumber"] == "" || data["phoneNumber"]?.count != 10) {
            alertUser(text: "The patient phone number must be 10 digits with no spaces, dashes, or parentheses.")
            return false;
        }
        //patientLocation is optional
        //patientDischargeDate is optional
        if (data["referralName"] == "") {
            alertUser(text: "The referral name is missing.")
            return false;
        }
        if (data["referralPhoneNumber"] == "" || data["referralPhoneNumber"]?.count != 10) {
            alertUser(text: "The referral phone number must be 10 digits with no spaces, dashes, or parentheses.")
            return false;
        }
        //referralOrg is optional
        return true;
    }
    
    func saveReferralName() {
        defaults.set(self.referralName.text!, forKey: "referralName")
    }
    
    func saveReferralPhoneNumber() {
        defaults.set(self.referralPhoneNumber.text!, forKey: "referralPhoneNumber")
    }
    
    func saveReferralOrg() {
        defaults.set(self.referralOrg.text!, forKey: "referralOrg")
    }
    
    func loadUserData() {
        if let referralName = defaults.string(forKey: "referralName") {
            self.referralName.text = referralName
        }
        if let referralPhoneNumber = defaults.string(forKey: "referralPhoneNumber") {
            self.referralPhoneNumber.text = referralPhoneNumber
        }
        if let referralOrg = defaults.string(forKey: "referralOrg") {
            self.referralOrg.text = referralOrg
        }
    }
    
    func submitRequest() {
        // 1. Collect data
        let json: [String: String] = [
            "firstName": Shared.shared.patientFirstName!,
            "lastName": Shared.shared.patientLastName!,
            "phoneNumber": Shared.shared.patientPhoneNumber!,
            "dischargeDate": Shared.shared.patientDischargeDate!,
            "location": Shared.shared.patientLocation!,
            "referralName": self.referralName.text!,
            "referralPhoneNumber": self.referralPhoneNumber.text!,
            "referralOrg": self.referralOrg.text!,
            "source": "iOS"
        ];
        
        // 2. Verify data is valid
        if (!isInformationValid(data: json)) {
            print("referral data is NOT valid: \(String(describing: json))")
            return
        }
        
        // 3. Create request object
        let url: String = "https://iha-referral.meteorapp.com/methods/createReferralAPI";
        //let url: String = "http://localhost:3000/methods/createReferralAPI";
        var request = URLRequest(url: URL(string: url)!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        request.httpBody = jsonData
        
        // 4. Send HTTP POST to API
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // 5. Display result from request
            //network error
            guard let data = data, error == nil else {
                self.errorResponse(error: error, message: "There was a networking error. Please ensure you are connected to the internet and try again.")
                return
            }
            
            //HTTP error
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                self.errorResponse(error: error, message: "There was error with the server. Please try again later.")
            } else {
                //successful response
                self.successfulResponse(data: data)
            }
        }
        task.resume()
    }
    
    @IBOutlet weak var referralName: UITextField!
    @IBOutlet weak var referralPhoneNumber: UITextField!
    @IBOutlet weak var referralOrg: UITextField!
    
    @IBAction func submitRequest(_ sender: Any) {
        self.submitRequest();
    }
    
    @IBAction func referralNameChanged(_ sender: UITextField) {
        saveReferralName()
    }
    
    @IBAction func referralPhoneNumberChanged(_ sender: UITextField) {
        saveReferralPhoneNumber()
    }
    
    @IBAction func referralOrgChanged(_ sender: UITextField) {
        saveReferralOrg()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load user referral data
        loadUserData()
    }
    
}
