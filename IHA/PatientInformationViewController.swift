//
//  PatientInformationViewController.swift
//  IHA
//
//  Created by Matthew Greci on 2/21/18.
//  Copyright © 2018 Matthew Greci. All rights reserved.
//

import UIKit

final class Shared {
    static let shared = Shared()
    
    var patientFirstName : String?
    var patientLastName : String?
    var patientPhoneNumber : String?
    var patientLocation : String?
    var patientDischargeDate : String?
    var referralName : String?
    var referralPhoneNumber : String?
    var referralOrg : String?

}

class PatientInformationViewController: UIViewController {
    @IBOutlet weak var patientFirstName: UITextField!
    @IBOutlet weak var patientLastName: UITextField!
    @IBOutlet weak var patientPhoneNumber: UITextField!
    
    @IBAction func cancelRequest(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        Shared.shared.patientFirstName = self.patientFirstName.text!
        Shared.shared.patientLastName = self.patientLastName.text!
        Shared.shared.patientPhoneNumber = self.patientPhoneNumber.text!
    }
    
}
