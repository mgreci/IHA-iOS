//
//  MainViewController.swift
//  IHA
//
//  Created by Matthew Greci on 2/1/18.
//  Copyright © 2018 Matthew Greci. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var houseCall: UIView!
    @IBOutlet weak var referral: UIView!
    @IBOutlet weak var website: UIView!
    @IBOutlet weak var scheduling: UIView!
    
    var IHAPhoneNumber = "6197385566"
    var IHAwebsite = "http://ihasd.com"
    
    func goToWebsite() {
        if let url = URL(string: IHAwebsite), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func callScheduling() {
        if let url = URL(string: "tel://\(IHAPhoneNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @objc func houseCallTap(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "houseCallNav")
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func referralTap(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "referralNav")
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func websiteTap(_ sender: UITapGestureRecognizer) {
        goToWebsite()
    }
    
    @objc func schedulingTap(_ sender: UITapGestureRecognizer) {
        callScheduling()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        houseCall.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.houseCallTap(_:))))
        
        referral.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.referralTap(_:))))
        
        website.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.websiteTap(_:))))
        
        scheduling.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.schedulingTap(_:))))
    }
}
